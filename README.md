# Atilla Multiboard

## Description

This repo hosts a multiboard containing informations about interesting points around EISTI school, gives information about the trains that will leave the closest train station, and some data about the weather.

## Requirements

Before using this multiboard with weather & station data, you must reference your API keys under two files:

1. `sncf_token` to store your sncf token (you must separate username and password with a new line)
2. `owm_token` to store your OpenWeatherMap 5days/3h forecast token

## Installation

1. Clone the project wherever you want on your FS.
2. `mkdir venv/rails`
3. `bundle install --path venv/rails`

## How to run 

In order to run the project, at least during development phase, run  `bundle exec ruby main.rb` and open your browser on `http://localhost:4567`

## Contributors

From an idea proposed by Atilla's members, project was started and made by Flavien Raynaud

require 'json'
require 'net/http'
require 'time'


class Bus
  attr_reader :busses

  def initialize
    from_stivo
  end

  private
  def from_stivo
    data = Bus.query_stivo(4, "40:234", "059440045:45")

    @busses = []

    data["lines"][0]["destinations"].each do |destination|
      times = []
      destination['times'].each do |time|
        times << (time["departurewait"]).to_i / 60
      end
      @busses << {:name => destination["display"], :times => times}
    end
  end

  def self.query_stivo(nb_results, stop_id, line_id)
    uri = URI("https://prod.instant-system.com/InstantCore/sttl/v2/GetXStopTimetables.json?key=8&stopareaid=#{stop_id}&nb=#{nb_results}&operatorid=stivo&lineid=#{line_id}")
    JSON.parse(Net::HTTP.get(uri)) rescue {}
  end
end